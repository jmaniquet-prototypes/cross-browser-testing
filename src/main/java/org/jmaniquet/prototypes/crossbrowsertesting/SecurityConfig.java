package org.jmaniquet.prototypes.crossbrowsertesting;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.logout.ForwardLogoutSuccessHandler;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
			.authorizeRequests()
				.antMatchers("/javax.faces.resource/**").permitAll()
				.anyRequest().authenticated()
			.and().formLogin()
				.loginPage("/login.xhtml")
				.loginProcessingUrl("/sign-in")
				.permitAll()
				.defaultSuccessUrl("/home")
				.failureUrl("/login.xhtml?error=true")
				.usernameParameter("login-input")
				.passwordParameter("password-input")
			.and().logout()
				.logoutUrl("/log-out")
				.logoutSuccessHandler(new ForwardLogoutSuccessHandler("/login.xhtml?logout=true"))
			.and().csrf().disable();
	}
}
