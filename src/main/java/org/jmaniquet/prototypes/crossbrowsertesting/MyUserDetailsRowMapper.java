package org.jmaniquet.prototypes.crossbrowsertesting;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;

class MyUserDetailsRowMapper implements RowMapper<UserDetails> {
	
	@Override
	public UserDetails mapRow(ResultSet rs, int rowNum) throws SQLException {
		String username = rs.getString("username");
		String password = rs.getString("password");
		return User
				.builder()
				.username(username)
				.password(password)
				.authorities(AuthorityUtils.NO_AUTHORITIES)
				.build();
	}
}