package org.jmaniquet.prototypes.crossbrowsertesting;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomeController {

	static final String URL_ROOT = "/";
	static final String URL_LOGIN = "/login";
	static final String URL_WELCOME = "/welcome";
	static final String URL_HOME = "/home";
	
	@GetMapping({URL_ROOT, URL_LOGIN, URL_WELCOME, URL_HOME})
	public String welcome() {
		return "/home.xhtml";
	}
}
