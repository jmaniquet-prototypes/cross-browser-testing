package org.jmaniquet.prototypes.crossbrowsertesting;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

public class MyUserDetailsService implements UserDetailsService {
	
	private static final Logger logger = LoggerFactory.getLogger(MyUserDetailsService.class);
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	private RowMapper<UserDetails> userDetailsRowMapper = new MyUserDetailsRowMapper();
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		List<UserDetails> users = jdbcTemplate.query(
				"select username, password from person where username = ?",
				userDetailsRowMapper, username);
		
		if (users.isEmpty()) {
			logger.debug("Query returned no results for user '{}'", username);
			throw new UsernameNotFoundException("User " + username + " not found");
		}
		
		return users.get(0);
	}

}
