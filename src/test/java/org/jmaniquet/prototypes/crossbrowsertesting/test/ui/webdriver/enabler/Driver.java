package org.jmaniquet.prototypes.crossbrowsertesting.test.ui.webdriver.enabler;

public enum Driver {
	CHROME("chrome"),
	FIREFOX("firefox");
	
	private final String profile;
	private Driver(String profile) {
		this.profile = profile;
	}
	public String getProfile() {
		return profile;
	}
}