package org.jmaniquet.prototypes.crossbrowsertesting.test.ui.webdriver.loggeduser;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.junit.jupiter.api.extension.ExtendWith;

@Target({ ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
@ExtendWith(WithLoggedUserExtension.class)
public @interface WithLoggedUser {
	
	static final String DEFAULT_PWD = "default-pwd";
	static final String DEFAULT_USER = "default-user";
	
	String username() default DEFAULT_USER;
	String password() default DEFAULT_PWD;
}
