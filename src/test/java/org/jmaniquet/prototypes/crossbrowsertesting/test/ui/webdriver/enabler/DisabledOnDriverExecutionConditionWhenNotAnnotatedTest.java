package org.jmaniquet.prototypes.crossbrowsertesting.test.ui.webdriver.enabler;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.SoftAssertions.assertSoftly;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ConditionEvaluationResult;
import org.junit.jupiter.api.extension.ExtensionContext;

public class DisabledOnDriverExecutionConditionWhenNotAnnotatedTest {

	private DisabledOnDriverExecutionCondition underTest = new DisabledOnDriverExecutionCondition();
	
	private ExtensionContext junitCtx;
	
	@BeforeEach
	public void setup() {
		junitCtx = mock(ExtensionContext.class);
	}
	
	@Test
	public void testDisabledOnDriverWhenNotAnnotated() {
		when(junitCtx.getElement()).thenReturn(Optional.of(String.class));
		
		ConditionEvaluationResult result = underTest.evaluateExecutionCondition(junitCtx);
		assertThat(result).isNotNull();
		
		assertSoftly(softly -> {
			softly.assertThat(result.isDisabled()).isFalse();
			softly.assertThat(result.getReason().get()).isEqualTo(String.format("Test is enabled since @%s is not present", DisabledOnDriver.class.getSimpleName()));
		});
	}
}
