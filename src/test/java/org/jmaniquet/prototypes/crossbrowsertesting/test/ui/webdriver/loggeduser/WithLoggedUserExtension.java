package org.jmaniquet.prototypes.crossbrowsertesting.test.ui.webdriver.loggeduser;

import java.lang.reflect.AnnotatedElement;
import java.util.Optional;

import org.apache.http.client.CookieStore;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.HttpClientContext;
import org.junit.jupiter.api.extension.AfterEachCallback;
import org.junit.jupiter.api.extension.BeforeEachCallback;
import org.junit.jupiter.api.extension.BeforeTestExecutionCallback;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.platform.commons.support.AnnotationSupport;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;

public class WithLoggedUserExtension implements BeforeEachCallback, BeforeTestExecutionCallback, AfterEachCallback {

	private static final Logger logger = LoggerFactory.getLogger(WithLoggedUserExtension.class);
	
	@Override
	public void beforeEach(ExtensionContext junitCtx) throws Exception {
		Optional<WithLoggedUser> optAnnotation = AnnotationSupport.findAnnotation(junitCtx.getElement(), WithLoggedUser.class);
		if(!optAnnotation.isPresent()) {
			return;
		}
		
		WithLoggedUser annotation = optAnnotation.get();
		String username = annotation.username();
		String password = annotation.password();
		
		ApplicationContext appCtx = SpringExtension.getApplicationContext(junitCtx);
		LoggedUserApi loggedUserApi = appCtx.getBean(LoggedUserApi.class);
		
		loggedUserApi.initUser(username, password);
	}

	@Override
	public void beforeTestExecution(ExtensionContext junitCtx) throws Exception {
		Optional<AnnotatedElement> optElement = junitCtx.getElement();
		Optional<WithLoggedUser> optAnnotation = AnnotationSupport.findAnnotation(optElement, WithLoggedUser.class);
		if(!optAnnotation.isPresent()) {
			return;
		}
		
		AnnotatedElement element = optElement.get();
		logger.debug("Autologging user for test method : {}", element);
		
		WithLoggedUser annotation = optAnnotation.get();
		String username = annotation.username();
		String password = annotation.password();
		
		ApplicationContext appCtx = SpringExtension.getApplicationContext(junitCtx);
		LoggedUserApi loggedUserApi = appCtx.getBean(LoggedUserApi.class);
		WebDriver webDriver = appCtx.getBean(WebDriver.class);
		
		HttpPost post = loggedUserApi.buildSignInRequest(username, password);
		
		// Create local HTTP context and bind custom cookie store to it
		HttpClientContext localContext = HttpClientContext.create();
		CloseableHttpResponse response = loggedUserApi.callSignInEndpoint(post, localContext);
		
		CookieStore cookieStore = localContext.getCookieStore();
		loggedUserApi.registerCookiesInBrowser(webDriver, cookieStore);

		loggedUserApi.closeResponse(response);
	}

	@Override
	public void afterEach(ExtensionContext junitCtx) throws Exception {
		Optional<WithLoggedUser> optAnnotation = AnnotationSupport.findAnnotation(junitCtx.getElement(), WithLoggedUser.class);
		if(!optAnnotation.isPresent()) {
			return;
		}
		
		WithLoggedUser annotation = optAnnotation.get();
		String username = annotation.username();
		
		ApplicationContext appCtx = SpringExtension.getApplicationContext(junitCtx);
		LoggedUserApi loggedUserApi = appCtx.getBean(LoggedUserApi.class);
		
		loggedUserApi.cleanUser(username);
	}

}
