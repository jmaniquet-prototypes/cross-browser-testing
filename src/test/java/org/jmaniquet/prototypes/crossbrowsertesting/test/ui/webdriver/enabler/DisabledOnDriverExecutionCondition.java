package org.jmaniquet.prototypes.crossbrowsertesting.test.ui.webdriver.enabler;

import static java.util.stream.Collectors.toList;
import static org.junit.jupiter.api.extension.ConditionEvaluationResult.enabled;

import java.lang.reflect.AnnotatedElement;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import org.junit.jupiter.api.extension.ConditionEvaluationResult;
import org.junit.jupiter.api.extension.ExecutionCondition;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.platform.commons.support.AnnotationSupport;
import org.springframework.context.ApplicationContext;
import org.springframework.core.env.Environment;

public class DisabledOnDriverExecutionCondition implements ExecutionCondition, SpringContextAware {
	
	@Override
	public ConditionEvaluationResult evaluateExecutionCondition(ExtensionContext junitCtx) {
		Optional<AnnotatedElement> element = junitCtx.getElement();
		Optional<DisabledOnDriver> optional = AnnotationSupport.findAnnotation(element, DisabledOnDriver.class);
		
		if (!optional.isPresent()) {
			String reason = String.format("Test is enabled since @%s is not present", DisabledOnDriver.class.getSimpleName());
			return enabled(reason);
		}
		
		ApplicationContext springCtx = getSpringContext(junitCtx);
		Environment springEnv = springCtx.getEnvironment();
		List<String> activeProfiles = List.of(springEnv.getActiveProfiles());
		List<String> targetedProfiles = Stream.of(optional.get().drivers())
				.map(driver -> driver.getProfile())
				.collect(toList());
		
		boolean disabled = targetedProfiles.stream().anyMatch(targetedProfile -> activeProfiles.contains(targetedProfile));
		return disabled ?
			ConditionEvaluationResult.disabled(optional.get().reason()) :
			ConditionEvaluationResult.enabled(
					String.format("Test is enabled since @%s is present but none of the targeted profiles (%s) were active",
					DisabledOnDriver.class.getSimpleName(), targetedProfiles));
	}
}
