package org.jmaniquet.prototypes.crossbrowsertesting.test.ui.webdriver.provider;

import org.openqa.selenium.WebDriver;

public interface WebDriverProvider {

	WebDriver webDriver();
}
