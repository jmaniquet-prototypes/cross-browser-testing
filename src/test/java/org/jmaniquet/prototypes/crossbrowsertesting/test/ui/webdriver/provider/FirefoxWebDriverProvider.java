package org.jmaniquet.prototypes.crossbrowsertesting.test.ui.webdriver.provider;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import io.github.bonigarcia.wdm.WebDriverManager;

@Configuration
@Profile("ui-integration-tests & firefox")
public class FirefoxWebDriverProvider implements WebDriverProvider {

	private static final Logger logger = LoggerFactory.getLogger(FirefoxWebDriverProvider.class);
	
	@Override
	@Bean
	public WebDriver webDriver() {
		logger.info("WebDriverManager - setup start - firefox");
		WebDriverManager manager = WebDriverManager.firefoxdriver();
		manager.setup();
		
		String version = manager.getDownloadedDriverVersion();
		logger.info("WebDriverManager - setup done - firefox - driver version : {}", version);
		
		logger.info("WebDriverManager - instanciating webdriver - firefox");
		return new FirefoxDriver();
	}

}
