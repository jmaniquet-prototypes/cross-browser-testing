package org.jmaniquet.prototypes.crossbrowsertesting.test.ui.webdriver.loggeduser;

import org.apache.http.client.CookieStore;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.HttpClientContext;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.DisposableBean;

interface LoggedUserApi extends DisposableBean {

	void initUser(String login, String pwd);
	
	void cleanUser(String login);
	
	HttpPost buildSignInRequest(String login, String pwd);
	
	CloseableHttpResponse callSignInEndpoint(HttpPost post, HttpClientContext localContext);
	
	void registerCookiesInBrowser(WebDriver webDriver, CookieStore cookieStore);
	
	void closeResponse(CloseableHttpResponse response);
}
