package org.jmaniquet.prototypes.crossbrowsertesting.test.ui.webdriver.loggeduser;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.http.NameValuePair;
import org.apache.http.client.CookieStore;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
class LoggedUserApiImpl implements LoggedUserApi {

	@Value("http://localhost:${server.port}")
	private String rootPath;

	private static final Logger logger = LoggerFactory.getLogger(LoggedUserApiImpl.class);
	
	@Autowired
	private CloseableHttpClient httpClient;
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	@Override
	public void initUser(String login, String pwd) {
		String encodedPwd = passwordEncoder.encode(pwd);
		
		logger.debug("Inserting user into db - {}, {}, {}", login, pwd, encodedPwd);
		jdbcTemplate.update("insert into person (username, password) values (?, ?)", login, encodedPwd);
	}
	
	@Override
	public void cleanUser(String login) {
		logger.debug("Removing user from db - {}", login);
		jdbcTemplate.update("delete from person where username = ?", login);
	}
	
	@Override
	public HttpPost buildSignInRequest(String login, String pwd) {
		logger.debug("Constructing post request");
		
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("login-input", login));
		params.add(new BasicNameValuePair("password-input", pwd));
		
		UrlEncodedFormEntity entity;
		try {
			entity = new UrlEncodedFormEntity(params);
		} catch (UnsupportedEncodingException e) {
			throw new RuntimeException("Erreur lors de l'initialisation de la requête http", e);
		}
		HttpPost httpPost = new HttpPost(rootPath + "/sign-in");
		httpPost.setEntity(entity);
		
		return httpPost;
	}

	@Override
	public CloseableHttpResponse callSignInEndpoint(HttpPost post, HttpClientContext localContext) {
		logger.debug("Calling sign-in endpoint");
		CloseableHttpResponse response = null;
		try {
			response = httpClient.execute(post, localContext);
			logger.debug("Response: {}", response.getStatusLine());
		} catch (IOException e) {
			throw new RuntimeException("Erreur lors de l'appel au endpoint d'authentification", e);
		}
		
		return response;
	}

	@Override
	public void registerCookiesInBrowser(WebDriver webDriver, CookieStore cookieStore) {
		List<Cookie> cookies = cookieStore.getCookies();
		
		logger.debug("Calling login page");
		webDriver.get(rootPath);
		
		logger.debug("Registering cookies on webdriver");
		for (Cookie cookie : cookies) {
			webDriver.manage().addCookie(new org.openqa.selenium.Cookie(cookie.getName(), cookie.getValue()));
		}
	}

	@SuppressWarnings("deprecation")
	@Override
	public void closeResponse(CloseableHttpResponse response) {
		logger.debug("Closing response");
		IOUtils.closeQuietly(response);
	}

	@Override
	public void destroy() throws Exception {
		logger.info("Closing Http Client");
		httpClient.close();
	}
}
