package org.jmaniquet.prototypes.crossbrowsertesting.test.ui.webdriver.enabler;

import org.junit.jupiter.api.extension.ExtensionContext;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;

public interface SpringContextAware {

	default ApplicationContext getSpringContext(ExtensionContext context) {
		return SpringExtension.getApplicationContext(context);
	}
}
