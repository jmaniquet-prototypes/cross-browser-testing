package org.jmaniquet.prototypes.crossbrowsertesting.test.ui.webdriver.loggeduser;

import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan
public class LoggedUserConfig {

	private static final Logger logger = LoggerFactory.getLogger(LoggedUserConfig.class);

	@Bean
	public CloseableHttpClient httpClient() {
		logger.info("Initializing Http Client");
		return HttpClients.createDefault();
	}
}
