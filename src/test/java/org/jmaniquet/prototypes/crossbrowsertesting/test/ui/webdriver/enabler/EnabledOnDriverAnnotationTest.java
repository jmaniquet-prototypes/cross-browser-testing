package org.jmaniquet.prototypes.crossbrowsertesting.test.ui.webdriver.enabler;

import static org.assertj.core.api.Assertions.assertThat;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.extension.Extension;
import org.junit.platform.commons.support.AnnotationSupport;

public class EnabledOnDriverAnnotationTest {

	@Test
	public void testEnabledOnDriverIsAnnotatedWithExtendWith() {
		Optional<ExtendWith> extendWith = AnnotationSupport.findAnnotation(EnabledOnDriver.class, ExtendWith.class);
		assertThat(extendWith).isPresent();
		
		Class<? extends Extension>[] extensions = extendWith.get().value();
		assertThat(extensions).containsExactlyInAnyOrder(EnabledOnDriverExecutionCondition.class);
	}

	@Test
	public void testEnabledOnDriverIsAnnotatedWithRetention() {
		Optional<Retention> retention = AnnotationSupport.findAnnotation(EnabledOnDriver.class, Retention.class);
		assertThat(retention).isPresent();
		
		RetentionPolicy policy = retention.get().value();
		assertThat(policy).isEqualTo(RetentionPolicy.RUNTIME);
	}
	
	@Test
	public void testEnabledOnDriverIsAnnotatedWithTarget() {
		Optional<Target> target = AnnotationSupport.findAnnotation(EnabledOnDriver.class, Target.class);
		assertThat(target).isPresent();
		
		ElementType[] elementTypes = target.get().value();
		assertThat(elementTypes).containsExactlyInAnyOrder(ElementType.TYPE, ElementType.METHOD);
	}
}
