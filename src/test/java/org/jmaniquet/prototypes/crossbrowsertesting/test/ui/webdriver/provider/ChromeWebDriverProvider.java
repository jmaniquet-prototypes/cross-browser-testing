package org.jmaniquet.prototypes.crossbrowsertesting.test.ui.webdriver.provider;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import io.github.bonigarcia.wdm.WebDriverManager;

@Configuration
@Profile("ui-integration-tests & chrome")
public class ChromeWebDriverProvider implements WebDriverProvider {

	private static final Logger logger = LoggerFactory.getLogger(ChromeWebDriverProvider.class);
	
	@Override
	@Bean
	public WebDriver webDriver() {
		logger.info("WebDriverManager - setup start - chrome");
		WebDriverManager manager = WebDriverManager.chromedriver();
		manager.setup();
		
		String version = manager.getDownloadedDriverVersion();
		logger.info("WebDriverManager - setup done - chrome - driver version : {}", version);
		
		ChromeOptions opt = new ChromeOptions()
				.setHeadless(true)
				.addArguments("--no-sandbox");
		
		logger.info("WebDriverManager - instanciating webdriver - chrome");
		return new ChromeDriver(opt);
	}
}
