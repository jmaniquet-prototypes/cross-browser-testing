package org.jmaniquet.prototypes.crossbrowsertesting.test.ui.webdriver.enabler;

import static org.assertj.core.api.Assertions.assertThat;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.extension.Extension;
import org.junit.platform.commons.support.AnnotationSupport;

public class DisabledOnDriverAnnotationTest {

	@Test
	public void testDisabledOnDriverIsAnnotatedWithExtendWith() {
		Optional<ExtendWith> extendWith = AnnotationSupport.findAnnotation(DisabledOnDriver.class, ExtendWith.class);
		assertThat(extendWith).isPresent();
		
		Class<? extends Extension>[] extensions = extendWith.get().value();
		assertThat(extensions).containsExactlyInAnyOrder(DisabledOnDriverExecutionCondition.class);
	}

	@Test
	public void testDisabledOnDriverIsAnnotatedWithRetention() {
		Optional<Retention> retention = AnnotationSupport.findAnnotation(DisabledOnDriver.class, Retention.class);
		assertThat(retention).isPresent();
		
		RetentionPolicy policy = retention.get().value();
		assertThat(policy).isEqualTo(RetentionPolicy.RUNTIME);
	}
	
	@Test
	public void testDisabledOnDriverIsAnnotatedWithTarget() {
		Optional<Target> target = AnnotationSupport.findAnnotation(DisabledOnDriver.class, Target.class);
		assertThat(target).isPresent();
		
		ElementType[] elementTypes = target.get().value();
		assertThat(elementTypes).containsExactlyInAnyOrder(ElementType.TYPE, ElementType.METHOD);
	}
}
