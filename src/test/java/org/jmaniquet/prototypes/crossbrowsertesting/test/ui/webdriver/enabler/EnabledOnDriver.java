package org.jmaniquet.prototypes.crossbrowsertesting.test.ui.webdriver.enabler;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.junit.jupiter.api.extension.ExtendWith;

@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@ExtendWith(EnabledOnDriverExecutionCondition.class)
public @interface EnabledOnDriver {
	
	Driver [] drivers();
	
	String reason() default "";
}
