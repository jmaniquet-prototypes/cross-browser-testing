package org.jmaniquet.prototypes.crossbrowsertesting.test.ui.webdriver.loggeduser;

import static org.assertj.core.api.Assertions.assertThat;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.extension.Extension;
import org.junit.platform.commons.support.AnnotationSupport;

public class WithLoggedUserTest {

	@Test
	public void testWithLoggedUserIsAnnotatedWithExtendWith() {
		Optional<ExtendWith> extendWith = AnnotationSupport.findAnnotation(WithLoggedUser.class, ExtendWith.class);
		assertThat(extendWith).isPresent();
		
		Class<? extends Extension>[] extensions = extendWith.get().value();
		assertThat(extensions).containsExactlyInAnyOrder(WithLoggedUserExtension.class);
	}

	@Test
	public void testWithLoggedUserIsAnnotatedWithRetention() {
		Optional<Retention> retention = AnnotationSupport.findAnnotation(WithLoggedUser.class, Retention.class);
		assertThat(retention).isPresent();
		
		RetentionPolicy policy = retention.get().value();
		assertThat(policy).isEqualTo(RetentionPolicy.RUNTIME);
	}
	
	@Test
	public void testEnabledOnChromeIsAnnotatedWithTarget() {
		Optional<Target> target = AnnotationSupport.findAnnotation(WithLoggedUser.class, Target.class);
		assertThat(target).isPresent();
		
		ElementType[] elementTypes = target.get().value();
		assertThat(elementTypes).containsExactlyInAnyOrder(ElementType.METHOD);
	}
}
