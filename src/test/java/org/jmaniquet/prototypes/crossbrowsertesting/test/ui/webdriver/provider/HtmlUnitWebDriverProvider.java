package org.jmaniquet.prototypes.crossbrowsertesting.test.ui.webdriver.provider;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@Profile("!ui-integration-tests")
public class HtmlUnitWebDriverProvider implements WebDriverProvider {
	
	private static final Logger logger = LoggerFactory.getLogger(HtmlUnitWebDriverProvider.class);
	
	@Override
	@Bean
	public WebDriver webDriver() {
		logger.info("Initiating HtmlUnitDriver");
		return new HtmlUnitDriver();
	}
}
