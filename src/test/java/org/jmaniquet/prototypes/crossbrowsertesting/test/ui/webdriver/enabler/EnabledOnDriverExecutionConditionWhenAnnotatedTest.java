package org.jmaniquet.prototypes.crossbrowsertesting.test.ui.webdriver.enabler;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.SoftAssertions.assertSoftly;
import static org.jmaniquet.prototypes.crossbrowsertesting.test.ui.webdriver.enabler.Driver.CHROME;
import static org.jmaniquet.prototypes.crossbrowsertesting.test.ui.webdriver.enabler.Driver.FIREFOX;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ConditionEvaluationResult;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.springframework.context.ApplicationContext;
import org.springframework.mock.env.MockEnvironment;

public class EnabledOnDriverExecutionConditionWhenAnnotatedTest {

	private EnabledOnDriverExecutionCondition dontUseDirectly = new EnabledOnDriverExecutionCondition();
	
	private ExtensionContext junitCtx;
	private ApplicationContext springCtx;
	private MockEnvironment springEnv;
	private EnabledOnDriverExecutionCondition underTest;
	
	@BeforeEach
	public void setup() {
		junitCtx = mock(ExtensionContext.class);
		springCtx = mock(ApplicationContext.class);
		springEnv = new MockEnvironment();
		
		underTest = spy(dontUseDirectly);
		doReturn(springCtx).when(underTest).getSpringContext(junitCtx);
		when(junitCtx.getElement()).thenReturn(Optional.of(DummyAnnotated.class));
		when(springCtx.getEnvironment()).thenReturn(springEnv);
	}
	
	@Test
	public void testEnabledOnDriverWhenProfileNotPresent() {
		ConditionEvaluationResult result = underTest.evaluateExecutionCondition(junitCtx);
		assertThat(result).isNotNull();
		
		assertSoftly(softly -> {
			softly.assertThat(result.isDisabled()).isTrue();
			softly.assertThat(result.getReason().get()).isEqualTo(String.format("Test is disabled since @%s is present but none of the targeted profiles ([firefox]) were active", EnabledOnDriver.class.getSimpleName()));
		});
	}
	
	@Test
	public void testEnabledOnDriverWhenProfilePresentAndNotAnnotated() {
		springEnv.setActiveProfiles(CHROME.getProfile());
		
		ConditionEvaluationResult result = underTest.evaluateExecutionCondition(junitCtx);
		assertThat(result).isNotNull();
		
		assertSoftly(softly -> {
			softly.assertThat(result.isDisabled()).isTrue();
			softly.assertThat(result.getReason().get()).isEqualTo(String.format("Test is disabled since @%s is present but none of the targeted profiles ([firefox]) were active", EnabledOnDriver.class.getSimpleName()));	
		});
	}
	
	@Test
	public void testEnabledOnDriverWhenProfilePresentAndAnnotated() {
		springEnv.setActiveProfiles(FIREFOX.getProfile());
		
		ConditionEvaluationResult result = underTest.evaluateExecutionCondition(junitCtx);
		assertThat(result).isNotNull();
		
		assertSoftly(softly -> {
			softly.assertThat(result.isDisabled()).isFalse();
			softly.assertThat(result.getReason().get()).isEqualTo(DummyAnnotated.DUMMY_REASON);	
		});
	}
	
	@EnabledOnDriver(drivers = FIREFOX, reason = DummyAnnotated.DUMMY_REASON)
	private static class DummyAnnotated {
		static final String DUMMY_REASON = "dummy-reason";
	}
}
