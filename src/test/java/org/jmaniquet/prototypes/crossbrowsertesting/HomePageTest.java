package org.jmaniquet.prototypes.crossbrowsertesting;

import static org.assertj.core.api.Assertions.assertThat;
import static org.jmaniquet.prototypes.crossbrowsertesting.HomeController.URL_HOME;
import static org.jmaniquet.prototypes.crossbrowsertesting.HomeController.URL_LOGIN;
import static org.jmaniquet.prototypes.crossbrowsertesting.HomeController.URL_ROOT;
import static org.jmaniquet.prototypes.crossbrowsertesting.HomeController.URL_WELCOME;

import org.jmaniquet.prototypes.crossbrowsertesting.test.ui.webdriver.loggeduser.WithLoggedUser;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EmptySource;
import org.junit.jupiter.params.provider.ValueSource;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;

@SpringBootTest(webEnvironment = WebEnvironment.DEFINED_PORT)
public class HomePageTest {
	
	@Autowired
	private WebDriver webDriver;
	
	@Value("http://localhost:${server.port}")
	private String rootPath;
	
	@ParameterizedTest
	@EmptySource
	@ValueSource(strings = {URL_ROOT, URL_LOGIN, URL_WELCOME, URL_HOME})
	@WithLoggedUser
	public void testArrivalOnHomePage(String t) throws Exception {
		webDriver.get(rootPath + t);
		assertThat(webDriver.getTitle()).isEqualTo("Nelson App - Home");
	}

	@ParameterizedTest
	@EmptySource
	@ValueSource(strings = {URL_ROOT, URL_LOGIN, URL_WELCOME, URL_HOME})
	@WithLoggedUser
	public void testLogOut(String t) throws Exception {
		webDriver.get(rootPath + t);
		assertThat(webDriver.getTitle()).isEqualTo("Nelson App - Home");
		
		webDriver.findElement(By.id("logout-btn")).click();
		assertThat(webDriver.getTitle()).isEqualTo("Nelson App - Login");
		
		WebElement infoIconSpan = webDriver.findElement(By.cssSelector("#messages-info.ui-messages div.ui-messages-info span.ui-messages-info-icon"));
		String infoIconText = infoIconSpan.getText();
		assertThat(infoIconText).isBlank();
		
		WebElement infoTextSpan = webDriver.findElement(By.cssSelector("#messages-info.ui-messages div.ui-messages-info span.ui-messages-info-summary"));
		String infoText = infoTextSpan.getText();
		assertThat(infoText).isEqualTo("You have been successfully disconnected");
	}
	
	@AfterEach
	public void teardown() {
		if (webDriver != null) {
			webDriver.quit();
		}
	}
}
