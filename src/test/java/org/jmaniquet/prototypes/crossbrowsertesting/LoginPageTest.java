package org.jmaniquet.prototypes.crossbrowsertesting;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.jmaniquet.prototypes.crossbrowsertesting.HomeController.URL_HOME;
import static org.jmaniquet.prototypes.crossbrowsertesting.HomeController.URL_LOGIN;
import static org.jmaniquet.prototypes.crossbrowsertesting.HomeController.URL_ROOT;
import static org.jmaniquet.prototypes.crossbrowsertesting.HomeController.URL_WELCOME;
import static org.jmaniquet.prototypes.crossbrowsertesting.test.ui.webdriver.enabler.Driver.CHROME;
import static org.jmaniquet.prototypes.crossbrowsertesting.test.ui.webdriver.enabler.Driver.FIREFOX;

import java.util.List;

import org.jmaniquet.prototypes.crossbrowsertesting.test.ui.webdriver.enabler.DisabledOnDriver;
import org.jmaniquet.prototypes.crossbrowsertesting.test.ui.webdriver.enabler.EnabledOnDriver;
import org.jmaniquet.prototypes.crossbrowsertesting.test.ui.webdriver.loggeduser.WithLoggedUser;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.EmptySource;
import org.junit.jupiter.params.provider.ValueSource;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;

@SpringBootTest(webEnvironment = WebEnvironment.DEFINED_PORT)
public class LoginPageTest {
	
	@Autowired
	private WebDriver webDriver;

	@Value("http://localhost:${server.port}")
	private String rootPath;
	
	@ParameterizedTest
	@EmptySource
	@ValueSource(strings = {URL_ROOT, URL_LOGIN, URL_WELCOME, URL_HOME})
	public void testArrivalOnLoginPage(String t) throws Exception {
		webDriver.get(rootPath + t);
		assertThat(webDriver.getTitle()).isEqualTo("Nelson App - Login");
		
		WebElement loginInput = webDriver.findElement(By.id("login-input"));
		assertThatIsInputText(loginInput);
		assertThatHasPlaceholder(loginInput, "Login");
		
		WebElement passwordInput = webDriver.findElement(By.id("password-input"));
		assertThatIsInputPassword(passwordInput);
		assertThatHasPlaceholder(passwordInput, "Password");
		
		WebElement loginBtn = webDriver.findElement(By.id("login-btn"));
		assertThatIsButtonSubmitWithLabel(loginBtn, "Login");
		
		WebElement registerLabel = webDriver.findElement(By.id("register-label"));
		assertThatIsLabelWithText(registerLabel, "No account ? You can always register :");
		
		WebElement registerBtn = webDriver.findElement(By.id("register-btn"));
		assertThatIsButtonLinkWithLabel(registerBtn, "Register");
		
		List<WebElement> messagesDiv = webDriver.findElements(By.id("messages"));
		assertThat(messagesDiv).isEmpty();
	}
	
	@ParameterizedTest
	@EmptySource
	@ValueSource(strings = {URL_ROOT, URL_LOGIN, URL_WELCOME, URL_HOME})
	@Sql(
		executionPhase = ExecutionPhase.BEFORE_TEST_METHOD,
		statements = "insert into person (username, password) values ('default-user', '{noop}default-pwd')")
	@Sql(
		executionPhase = ExecutionPhase.AFTER_TEST_METHOD,
		statements = "delete from person")
	public void testSignInWithValidCredentials(String t) {
		webDriver.get(rootPath + t);
		assertThat(webDriver.getTitle()).isEqualTo("Nelson App - Login");
		
		webDriver.findElement(By.id("login-input")).sendKeys(WithLoggedUser.DEFAULT_USER);
		webDriver.findElement(By.id("password-input")).sendKeys(WithLoggedUser.DEFAULT_PWD);
		webDriver.findElement(By.id("login-btn")).click();
		
		assertThat(webDriver.getTitle()).isEqualTo("Nelson App - Home");
		
		WebElement loginBtn = webDriver.findElement(By.id("logout-btn"));
		assertThatIsButtonSubmitWithLabel(loginBtn, "Logout");
		
		List<WebElement> messagesDiv = webDriver.findElements(By.id("messages"));
		assertThat(messagesDiv).isEmpty();
	}
	
	@ParameterizedTest
	@EmptySource
	@ValueSource(strings = {" ", "  "})
	public void testSignInWithEmptyOrBlankLogin(String login) {
		testSignInWithInvalidCredentials(login, WithLoggedUser.DEFAULT_PWD);
	}
	
	@Test
	@DisabledOnDriver(drivers = {CHROME, FIREFOX}, reason = "With some drivers, passing null to sendKeys throws an IllegalArgumentException")
	public void testSignInWithNullLogin1() {
		testSignInWithInvalidCredentials(null, WithLoggedUser.DEFAULT_PWD);
	}
	
	@Test
	@EnabledOnDriver(drivers = {CHROME, FIREFOX}, reason = "With some driver, passing null to sendKeys throws an IllegalArgumentException")
	public void testSignInWithNullLogin2() {
		assertThatExceptionOfType(IllegalArgumentException.class)
			.isThrownBy(() -> testSignInWithInvalidCredentials(null, WithLoggedUser.DEFAULT_PWD))
			.withMessage("Keys to send should be a not null CharSequence");
	}
	
	@ParameterizedTest
	@EmptySource
	@ValueSource(strings = {" ", "  "})
	public void testSignInWithEmptyOrBlankPwd(String password) {
		testSignInWithInvalidCredentials(WithLoggedUser.DEFAULT_USER, password);
	}
	
	@Test
	@DisabledOnDriver(drivers = {CHROME, FIREFOX}, reason = "With some drivers, passing null to sendKeys throws an IllegalArgumentException")
	public void testSignInWithNullPwd1() {
		testSignInWithInvalidCredentials(WithLoggedUser.DEFAULT_USER, null);
	}
	
	@Test
	@EnabledOnDriver(drivers = {CHROME, FIREFOX}, reason = "With some driver, passing null to sendKeys throws an IllegalArgumentException")
	public void testSignInWithNullPwd2() {
		assertThatExceptionOfType(IllegalArgumentException.class)
			.isThrownBy(() -> testSignInWithInvalidCredentials(WithLoggedUser.DEFAULT_USER, null))
			.withMessage("Keys to send should be a not null CharSequence");
	}
	
	@ParameterizedTest
	@CsvSource({
		WithLoggedUser.DEFAULT_USER + ", bad-pwd", // Valid login but invalid password
		"bad-user, " + WithLoggedUser.DEFAULT_PWD, // Invalid login
	})
	public void testSignInWithInvalidCredentials(String login, String password) {
		webDriver.get(rootPath);
		assertThat(webDriver.getTitle()).isEqualTo("Nelson App - Login");
		
		webDriver.findElement(By.id("login-input")).sendKeys(login);
		webDriver.findElement(By.id("password-input")).sendKeys(password);
		webDriver.findElement(By.id("login-btn")).click();
		
		assertThat(webDriver.getTitle()).isEqualTo("Nelson App - Login");
		
		WebElement errorIconSpan = webDriver.findElement(By.cssSelector("#messages.ui-messages div.ui-messages-error span.ui-messages-error-icon"));
		String errorIconText = errorIconSpan.getText();
		assertThat(errorIconText).isBlank();
		
		WebElement errorTextSpan = webDriver.findElement(By.cssSelector("#messages.ui-messages div.ui-messages-error span.ui-messages-error-summary"));
		String errorText = errorTextSpan.getText();
		assertThat(errorText).isEqualTo("Invalid login/password");
	}
	
	@AfterEach
	public void teardown() {
		if (webDriver != null) {
			webDriver.quit();
		}
	}
	
	private static void assertThatHasCss(WebElement element, String ... expectedCssClasses) {
		String attribute = element.getAttribute("class");
		assertThat(attribute).contains(expectedCssClasses);
	}
	
	private static void assertThatHasPlaceholder(WebElement element, String expectedPlaceholder) {
		String attribute = element.getAttribute("placeholder");
		assertThat(attribute).isEqualTo(expectedPlaceholder);
	}
	
	private static void assertThatIsButtonWithLabel(WebElement element, String expectedLabel) {
		assertThatTagIs(element, "button");
		assertThatHasCss(element, "ui-button");
		
		WebElement span = element.findElement(By.cssSelector(".ui-button-text.ui-c"));
		String text = span.getText();
		assertThat(text).isEqualTo(expectedLabel);
	}	
	
	private static void assertThatIsButtonLinkWithLabel(WebElement element, String expectedLabel) {
		assertThatIsButtonWithLabel(element, expectedLabel);
		assertThatIsOfType(element, "button");
	}
	
	private static void assertThatIsButtonSubmitWithLabel(WebElement element, String expectedLabel) {
		assertThatIsButtonWithLabel(element, expectedLabel);
		assertThatIsOfType(element, "submit");
	}
	
	private static void assertThatIsInputPassword(WebElement element) {
		assertThatTagIs(element, "input");
		assertThatIsOfType(element, "password");
		assertThatHasCss(element, "ui-inputfield", "ui-password");
	}
	
	private static void assertThatIsLabel(WebElement element) {
		assertThatTagIs(element, "label");
		assertThatHasCss(element, "ui-outputlabel");
	}
	
	private static void assertThatIsLabelWithText(WebElement element, String expectedText) {
		assertThatIsLabel(element);
		String text = element.getText();
		assertThat(text).isEqualTo(expectedText);
	}
	
	private static void assertThatIsInputText(WebElement element) {
		assertThatTagIs(element, "input");
		assertThatIsOfType(element, "text");
		assertThatHasCss(element, "ui-inputfield", "ui-inputtext");
	}
	
	private static void assertThatIsOfType(WebElement element, String expectedType) {
		String type = element.getAttribute("type");
		assertThat(type).isEqualTo(expectedType);
	}
	
	private static void assertThatTagIs(WebElement element, String expectedTag) {
		String tag = element.getTagName();
		assertThat(tag).isEqualTo(expectedTag);
	}
}
